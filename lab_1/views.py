# <<<<<<< HEAD
# import datetime
# now = datetime.datetime.now()
# # Enter your name here
# mhs_name = 'Misael Jonathan' # TODO Implement this

# # Create your views here.
# def index(request):
#     response = {'name': mhs_name, 'age': calculate_age(1998)}
#     return render(request, 'index.html', response)
from datetime import datetime, date
from django.shortcuts import render
# Enter your name here
mhs_name = 'Misael Jonathan' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 1, 17) #TODO Implement this, format (Year, Month, Date)
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year)}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

