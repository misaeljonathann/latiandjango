// FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '253292715196751',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11' //MUNGKIN PAKE {}
    });

    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
        }
        render(response);
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/all.js"; //CHECK SDK.JS -> ALL.JS
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    // console.log(loginFlag.status === "connected")
    if (loginFlag.status === "connected") {
      // console.log("masuk")
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        console.log("getUserData go")
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
          '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
          '<button class="postStatus" onclick="postStatus()">Post ke Fakebook</button>'
        );

        $('#kategori-navbar').append(
          '<a class="navbar-brand" href="" style="color:white">Fakebook</a>'+
          '<a class="navbar-brand" href="" style="color:white">Home</a>'+
          '<button class="navbar-brand" onclick="facebookLogout()" style="background-color:#3b5898; color:white; border-width:0px;">Logout</button>'
        );


        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            // console.log("bawah vvvvvv")
            // console.log(value)
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                  '<h2>' + value.story   + '</h2>' +
                  '<p>' + value.created_time.substring(0,10) + '</p>' +
                  // '<hr>'+
                '</div>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                  '<p>' + value.created_time.substring(0,10) + '</p>' +
                  // '<hr>'+
                '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h2>' + value.story + '</h2>' +
                  '<p>' + value.created_time.substring(0,10) + '</p>' +
                  // '<hr>'+
                '</div>'
              );
            }
            button = '<button id="' + value.id + '" onclick="deleteFeed(event)"> Delete </button>';
            $('#lab8').append(button + '<hr>');

          });
        });
      });
    } else {
      // Tampilan ketika belum login
      console.log("fail")
      $('#lab8').html('<div class="feed">' +
                '<h2>Selamat datang di Fakebook</h2>' +
                '<p>Silahkan login dengan menekan tombol dibawah ini</p>' +
                '<hr>'+'<button class="myButton" onclick="facebookLogin()">Login with Fakebook</button>'+
              '</div>'
            );
    }
//     <fb:login-button
//   scope="public_profile,email"
//   onlogin="checkLoginState();">
// </fb:login-button>
  };

  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function(response){
       console.log(response);
       render(response)
     }, {scope:'email, public_profile, user_posts, user_about_me, publish_actions, publish_pages'})
   }


  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout(function(response){
             console.log(response);
             render(response)
           });
        }
     });

  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = (fun) => {
    // ...'/me?fields=id,name,picture,relationship_status'
    FB.getLoginStatus(function(response) {
      console.log("getUserData Connect ? ")
        if (response.status === 'connected') {
          console.log("logged in, getUserData");
          FB.api('/me?fields=id,name,picture,gender,about,email,cover', 'GET', function (response){
            fun(response);
          });
        }
        // console.log("luar")
    });
  };


  const getUserFeed = (fun) => {
    FB.api("/me/feed", function (response) {
      if (response && !response.error) {
        /* handle the result */
        fun(response);
      }
    }
);
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
  };

  const deleteFeed = (e) => {
  console.log(e.target)
  FB.api(
    "/"+e.target.id+"",
    "DELETE",
    function (response) {
      console.log(response)
      if (response && !response.error) {
        alert('This post is deleted');
        e.target.parentNode.style.display = 'none';
        window.location.reload();
      }
    }
  );
};

  const postFeed = (post) => {
    FB.api("/me/feed", "post", {message: post}, function (response) {
      if (!response || response.error) {
       console.log('Error occured');
     } else {
       console.log('Post ID: ' + response.id);
     }
     window.location.reload();


  });

    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
  };
