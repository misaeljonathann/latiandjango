from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.forms.models import model_to_dict as model_to_dic
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()
def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()

    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    paginator = Paginator(mahasiswa_list, 25) # Show 25 contacts per page

    page = request.GET.get('page')

    try:
        mahasiswalist = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        mahasiswalist = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        mahasiswalist = paginator.page(paginator.num_pages)

    response = {"mahasiswa_list": mahasiswalist, "friend_list": friend_list}
    return render(request, html, response)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        if (Friend.objects.filter(npm__iexact=npm).exists()):
            raise Http404()
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        data = model_to_dict(friend)
        return HttpResponse(data)

def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponseRedirect('/lab-7/')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        # 'is_taken': Friend.objects.filter(npm__icontains=npm).exists() #RECHECK NYET
        'is_taken': Friend.objects.filter(npm__iexact=npm).exists()
        # if 'is_taken'== False:
        #     alert('false !')
        # else:
        #     alert('true !')
        #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

def friend_list_json(request):
    friends = Friend.objects.all()
    friend_list = []

    for friend in friends:
        friend_list.append(model_to_dic(friend))

    daftar = {'friend_list' : friend_list}
    return JsonResponse(daftar)

def view_friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar-teman.html'
    return render(request, html, response)

@csrf_exempt
def delete_friend(request):
    if request.method == 'POST':
        friend_id = request.POST['id']
        Friend.objects.filter(id=friend_id).delete()
        data = {'id' : friend_id}
        return JsonResponse(data)
