from django.conf.urls import url
from .views import index, add_friend, validate_npm, delete_friend, friend_list_json, view_friend_list

urlpatterns = [
     url(r'^$', index, name='index'),
     url(r'^add-friend/$', add_friend, name='add-friend'),
     url(r'^validate-npm/$', validate_npm, name='validate-npm'),
     url(r'^delete-friend/$', delete_friend, name='delete_friend'),
     url(r'^get-friend-list/$', friend_list_json, name='get-friend-list'),
     url(r'^view-friend-list/$', view_friend_list, name ='view_friend_list'),
]
